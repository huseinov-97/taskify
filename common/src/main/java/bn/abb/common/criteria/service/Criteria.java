package bn.abb.common.criteria.service;

public interface Criteria {

    Criteria copy();

}
