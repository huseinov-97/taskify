package bn.abb.task.dto.user;

import bn.abb.task.service.validation.ValidPassword;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegisterRequestDto {


    private String name;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    private Set<String> authority;

    @NotBlank
    @ValidPassword
    private String password;

    private String organizationName;

    private String phoneNumber;

    private String address;

}