package bn.abb.task.dto.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
public class UserRequest {
    private String name;

    private String surname;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;


}
