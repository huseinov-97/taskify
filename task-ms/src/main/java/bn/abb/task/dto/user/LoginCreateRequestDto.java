package bn.abb.task.dto.user;


import bn.abb.task.service.validation.ValidPassword;

import javax.validation.constraints.NotBlank;


public class LoginCreateRequestDto {
    @NotBlank
    private String email;

    @NotBlank
    @ValidPassword
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}