package bn.abb.task.dto.organization;


import bn.abb.task.dto.user.UserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrgWithUsersResponseDto {

    private Long id;
    private String name;
    private Set<UserDto> users;
}
