package bn.abb.task.exception.organization;


import bn.abb.common.exception.InvalidStateException;

public class OrganizationAlreadyExistException extends InvalidStateException {

    public static final String MESSAGE = "User with id %s is already in the organization.";
    private static final long serialVersionUID = 5843213248811L;

    public OrganizationAlreadyExistException(Long userId) {
        super(String.format(MESSAGE, userId));
    }
}
