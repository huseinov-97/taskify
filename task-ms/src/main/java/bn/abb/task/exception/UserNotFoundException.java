package bn.abb.task.exception;


import bn.abb.common.exception.NotFoundException;


public class UserNotFoundException extends NotFoundException {

    public static final String USER_NOT_FOUND = "User Not Found";

    public static final String MESSAGE = "User %s does not exist.";
    private static final long serialVersionUID = 5843213248811L;

    public UserNotFoundException(Long id) {
        super(String.format(MESSAGE, id));
    }

    public UserNotFoundException() {
        super(USER_NOT_FOUND);
    }

}
