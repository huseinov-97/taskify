package bn.abb.task.service;


import bn.abb.common.dto.GenericSearchDto;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

public interface TaskService {

    TaskResponseDto create(TaskCreateRequestDto requestDto, UserDetails userDetails);

    void delete(Long id);

    Page<TaskResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable);

}

