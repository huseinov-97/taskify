package bn.abb.task.service.impl;


import bn.abb.common.exception.NotFoundException;
import bn.abb.task.domain.Authority;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.domain.enumeration.AuthEnum;
import bn.abb.task.dto.user.RegisterUserDto;
import bn.abb.task.dto.user.UserDto;
import bn.abb.task.exception.EmailAlreadyUsedException;
import bn.abb.task.exception.UserNotFoundException;
import bn.abb.task.mapper.UserMapper;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.security.SecurityService;
import bn.abb.task.service.UserService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

import static bn.abb.task.domain.enumeration.AuthEnum.USER;

@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private AuthorityRepository authRepository;
    private PasswordEncoder passwordEncoder;
    private Organization organization;
    private UserMapper userMapper;
    private SecurityService securityService;


    @Override
    public void create(RegisterUserDto dto) {
        userRepository.findByEmail(dto.getEmail())
                .ifPresent(user -> {
                    throw new EmailAlreadyUsedException(dto.getEmail());
                });
        User user = createUserEntityObject(dto);
        userRepository.save(user);
    }


    private User createUserEntityObject(RegisterUserDto registerUserDto) {
        UserDto userEntity = getCurrentUser();
        User user = userMapper.dtoToEntity(registerUserDto);
        user.setPassword(passwordEncoder.encode(registerUserDto.getPassword()));
        Set<Authority> authEntities = new HashSet<>();
        Authority.builder().id(1).name(AuthEnum.USER).build();
        Authority userRole = authRepository.findByName(USER)
                .orElseThrow(() -> new NotFoundException("Not found Role"));
        authEntities.add(userRole);
        user.setAuthorities(authEntities);
        user.setOrganization(userEntity.getOrganization());
        return user;
    }


    @Override
    public UserDto getCurrentUser() {
        return userMapper.entityToDto(getCurrentUserByUsername(getCurrentLogin()));
    }


    private User getCurrentUserByUsername(String username) {
        return userRepository.findByEmail(username).orElseThrow(UserNotFoundException::new);
    }

    private String getCurrentLogin() {
        return securityService.getCurrentUserLogin().orElseThrow(UserNotFoundException::new);
    }
}

