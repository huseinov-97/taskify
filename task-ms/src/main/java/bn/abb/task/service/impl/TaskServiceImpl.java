package bn.abb.task.service.impl;

import bn.abb.common.dto.GenericSearchDto;
import bn.abb.common.search.SearchSpecification;
import bn.abb.task.domain.Task;
import bn.abb.task.domain.User;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import bn.abb.task.exception.NameMustBeUniqueException;
import bn.abb.task.exception.TaskNotFoundException;
import bn.abb.task.repository.TaskRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final ModelMapper modelMapper;
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;


    @Override
    @Transactional
    public TaskResponseDto create(TaskCreateRequestDto requestDto, UserDetails userDetails) {
        Task task = modelMapper.map(requestDto, Task.class);
        taskRepository.findByTitle(requestDto.getTitle())
                .ifPresent(organization -> {
                    throw new NameMustBeUniqueException(requestDto.getTitle());
                });
        Optional<User> user = userRepository.findByEmail(userDetails.getUsername());
        if (user.isPresent()) {
            task.setTitle(requestDto.getTitle());
            task.setDescription(requestDto.getDescription());
            task.setDeadline(requestDto.getDeadline());
            task.setStatus(requestDto.getStatus());
            task.setOrganization(user.get().getOrganization());
            taskRepository.save(task);
        }
        return modelMapper.map(taskRepository.save(task), TaskResponseDto.class);
    }


    @Override
    @Transactional
    public void delete(Long id) {
        log.trace("Remove task with id {}", id);
        taskRepository.delete(taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException(id)));
    }

    @Override
    public Page<TaskResponseDto> search(GenericSearchDto genericSearchDto, Pageable pageable) {
        return taskRepository
                .findAll(new SearchSpecification<>(genericSearchDto.getCriteria()), pageable)
                .map(task -> modelMapper.map(task, TaskResponseDto.class));
    }
}
