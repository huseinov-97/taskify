package bn.abb.task.service;


import bn.abb.task.dto.organization.AddUserToOrgDto;
import bn.abb.task.dto.organization.OrgWithUsersResponseDto;
import bn.abb.task.dto.organization.OrganizationCreateRequestDto;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;

public interface OrganizationService {

    OrganizationResponseInfoDto create(OrganizationCreateRequestDto requestDto);

    void addUserToOrganization(AddUserToOrgDto registerOrgUser, Long id);

    void removeUserFromOrganization(AddUserToOrgDto registerOrgUser, Long id);

}
