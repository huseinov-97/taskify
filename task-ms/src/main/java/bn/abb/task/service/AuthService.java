package bn.abb.task.service;

import bn.abb.task.domain.User;
import bn.abb.task.dto.user.LoginCreateRequestDto;
import bn.abb.task.dto.user.RegisterRequestDto;
import org.springframework.http.ResponseEntity;

public interface AuthService {

    User registerUser(RegisterRequestDto regRequest);

    ResponseEntity<?> login(LoginCreateRequestDto loginRequest);
}
