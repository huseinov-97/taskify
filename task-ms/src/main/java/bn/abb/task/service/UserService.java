package bn.abb.task.service;


import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.dto.user.RegisterUserDto;
import bn.abb.task.dto.user.UserDto;

public interface UserService {

     void create(RegisterUserDto dto) ;

     public UserDto getCurrentUser();
}
