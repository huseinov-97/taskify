package bn.abb.task.service.impl;

import bn.abb.task.domain.Authority;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.domain.enumeration.AuthEnum;
import bn.abb.task.dto.token.TokenResponse;
import bn.abb.task.dto.user.LoginCreateRequestDto;
import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.exception.EmailAlreadyUsedException;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.security.JwtUtils;
import bn.abb.task.service.AuthService;
import bn.abb.task.service.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {
    private final UserRepository userRepository;
    private final AuthorityRepository authRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtUtils jwtUtils;
    private final OrganizationRepository organizationRep;

    @Autowired
    AuthenticationManager authenticationManager;


    AuthServiceImpl(UserRepository userRepository, AuthorityRepository authRepository, PasswordEncoder passwordEncoder, JwtUtils jwtUtils, OrganizationRepository organizationRep) {
        this.userRepository = userRepository;
        this.authRepository = authRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtUtils = jwtUtils;
        this.organizationRep = organizationRep;
    }

    public User registerUser(RegisterRequestDto regRequest) {
        User user = new User(regRequest.getName(), regRequest.getEmail(), passwordEncoder.encode(regRequest.getPassword()));
        Set<Authority> authorities = new HashSet<>();

        authRepository.save(Authority.builder().name(AuthEnum.ADMIN).build());
        authRepository.save(Authority.builder().name(AuthEnum.USER).build());


        if (userRepository.existsByEmail(regRequest.getEmail())) {
            throw new EmailAlreadyUsedException("Email already in use : " + regRequest.getEmail());
        }

        Authority adminAuthority = authRepository.findByName(AuthEnum.ADMIN)
                .orElseThrow(() -> new RuntimeException("Error: Authority is not found."));
        authorities.add(adminAuthority);

        user.setAuthorities(authorities);
        userRepository.save(user);

        Organization organization = new Organization(regRequest.getOrganizationName(), user);
        organizationRep.save(organization);
        user.setOrganization(organization);
        userRepository.save(user);
        return user;
    }


    public ResponseEntity<?> login(LoginCreateRequestDto loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> authorities = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        ResponseEntity<TokenResponse> tokenResponseEntity = ResponseEntity.ok(new TokenResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                authorities));

        return tokenResponseEntity;
    }
}
