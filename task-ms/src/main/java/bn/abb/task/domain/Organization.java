package bn.abb.task.domain;

import bn.abb.task.repository.OrganizationRepository;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Organization.TABLE_NAME)
public class Organization {

    public static final String TABLE_NAME = "organizations";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "organization_id")
    private Set<User> user;


    public Organization(String organizationName, User user) {
        this.name=organizationName;
        this.user=new HashSet<>();
    }
}
