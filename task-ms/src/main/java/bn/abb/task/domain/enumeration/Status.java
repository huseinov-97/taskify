package bn.abb.task.domain.enumeration;


public enum Status {
    OPEN,
    IN_PROGRESS,
    DONE,
}
