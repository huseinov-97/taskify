package bn.abb.task.domain;

import bn.abb.task.domain.enumeration.AuthEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = Authority.TABLE_NAME)
public class Authority {

    public static final String TABLE_NAME = "authorities";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private AuthEnum name;

}
