package bn.abb.task.mapper;



import bn.abb.task.domain.Organization;
import bn.abb.task.dto.organization.OrganizationCreateRequestDto;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring", unmappedTargetPolicy = IGNORE)
public interface OrganizationMapper {

    Organization requestToOrganization(OrganizationCreateRequestDto organizationRequestDto);

    OrganizationResponseInfoDto organizationToDto(Organization organization);

}
