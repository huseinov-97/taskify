package bn.abb.task.web.rest;


import bn.abb.task.dto.token.InteractionResponse;
import bn.abb.task.dto.user.LoginCreateRequestDto;
import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.security.JwtUtils;
import bn.abb.task.service.impl.AuthServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    UserRepository userRepository;

    AuthServiceImpl authService;

    @Autowired
    AuthorityRepository authRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;

    public AuthController(AuthServiceImpl authService) {
        this.authService = authService;
    }


    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequestDto regRequest) {
        authService.registerUser(regRequest);
        return ResponseEntity.ok(new InteractionResponse("User registered successfully!"));
    }


    @PostMapping("/login")
    public ResponseEntity<?> login(@Valid @RequestBody LoginCreateRequestDto loginRequest) {
        return authService.login(loginRequest);
    }
}