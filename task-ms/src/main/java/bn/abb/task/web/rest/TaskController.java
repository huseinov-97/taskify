package bn.abb.task.web.rest;

import bn.abb.common.dto.GenericSearchDto;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import bn.abb.task.service.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/tasks")
public class TaskController {

    private final TaskService taskService;


    @PostMapping
    public ResponseEntity<TaskResponseDto> create(@RequestBody @Valid TaskCreateRequestDto requestDto,
                                                  @RequestParam UserDetails userDetails) {
        return ResponseEntity.status(HttpStatus.CREATED).body(taskService.create(requestDto, userDetails));
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.trace("Removing task with id {}", id);
        taskService.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/search")
    public ResponseEntity<Slice<TaskResponseDto>> search(@RequestBody GenericSearchDto genericSearchDto,
                                                         Pageable pageable) {
        log.trace("Search tasks request");
        return ResponseEntity.ok(taskService.search(genericSearchDto, pageable));
    }

}

