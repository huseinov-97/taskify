package bn.abb.task.repository;



import bn.abb.task.domain.Authority;
import bn.abb.task.domain.enumeration.AuthEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findByName(AuthEnum name);
}
