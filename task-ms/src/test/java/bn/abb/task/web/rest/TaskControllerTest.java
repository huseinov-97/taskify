package bn.abb.task.web.rest;

import bn.abb.common.dto.GenericSearchDto;
import bn.abb.task.domain.enumeration.TaskStatus;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import bn.abb.task.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDateTime;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(TaskController.class)
class TaskControllerTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final String BASE_URL = "/tasks";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final LocalDateTime DUMMY_DATETIME = LocalDateTime.parse("2015-02-20T06:30:00");


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TaskService taskService;


    private UserDetails userDetails;
    private TaskResponseDto taskResponseDto;
    private TaskCreateRequestDto taskCreateRequestDto;

    @BeforeEach
    void setUp() {

        taskResponseDto = TaskResponseDto
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .build();

        taskCreateRequestDto = TaskCreateRequestDto
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .build();
    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenCreateThenReturnOk() throws Exception {
        //Arrange
        when(taskService.create(taskCreateRequestDto, userDetails)).thenReturn(taskResponseDto);

        //Act
        mockMvc.perform(post(BASE_URL)
                        .content(objectMapper.writeValueAsString(taskCreateRequestDto))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        //verify
        verify(taskService, times(1)).create(taskCreateRequestDto, userDetails);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidIdWhenDeleteThenNoContent() throws Exception {
        //Action
        ResultActions actions = mockMvc.perform(delete(BASE_URL + "/" + DUMMY_ID)
                .contentType(MediaType.APPLICATION_JSON));

        //Assert
        verify(taskService, times(1)).delete(DUMMY_ID);
        actions.andExpect(status().isNoContent());
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidInputWhenSearchThenOk() throws Exception {
        //Arrange
        final Page<TaskResponseDto> payerDtoPage = Page.empty();
        when(taskService.search(any(GenericSearchDto.class), any(Pageable.class))).thenReturn(payerDtoPage);

        //Act
        mockMvc.perform(post(BASE_URL + "/search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(payerDtoPage)));

        //verify
        verify(taskService, times(1))
                .search(any(GenericSearchDto.class), any(Pageable.class));
    }
}
