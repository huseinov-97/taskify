package bn.abb.task.web.rest;

import bn.abb.task.dto.organization.AddUserToOrgDto;
import bn.abb.task.dto.organization.OrganizationCreateRequestDto;
import bn.abb.task.dto.organization.OrganizationResponseInfoDto;
import bn.abb.task.service.OrganizationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
@WebMvcTest(OrganizationController.class)
class OrganizationControllerTest {

    private static final String MAIN_URL = "/organizations";
    private static final String NAME = "ABB";
    private static final Long DUMMY_ID = 1L;
    private static final String ROLE_ADMIN = "ADMIN";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private OrganizationService orgService;

    @Qualifier("jacksonObjectMapper")
    @Autowired
    private ObjectMapper mapper;

    @Mock
    private OrganizationCreateRequestDto organizationCreateRequestDto;
    private OrganizationResponseInfoDto orgResponseInfoDto;
    private AddUserToOrgDto addUserToOrgDto;


    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();

        organizationCreateRequestDto = getOrgRequestDto();

        orgResponseInfoDto = getOrgResponseInfoDto();

        addUserToOrgDto = getRegisterOrgUse();

    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    public void givenRegisterOrgRequestDtoWhenCreateOrgThenSuccess() throws Exception {
        //Arrange
        when(orgService.create(organizationCreateRequestDto)).thenReturn(orgResponseInfoDto);

        //Act
        mockMvc.perform(post(MAIN_URL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(convertObjectToString(organizationCreateRequestDto)))
                .andExpect(status().isCreated())
                .andExpect(content().json(convertObjectToString(orgResponseInfoDto)));

        //Assert
        verify(orgService, times(1)).create(organizationCreateRequestDto);
    }


    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidIdWhenDeleteOrgUserThenNoContent() throws Exception {
        //Arrange
        doNothing().when(orgService).removeUserFromOrganization(addUserToOrgDto, DUMMY_ID);

        //Act
        mockMvc.perform(delete(MAIN_URL + "/user/" + DUMMY_ID)
                        .content(convertObjectToString(addUserToOrgDto))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());

        //Assert
        verify(orgService, times(1)).removeUserFromOrganization(addUserToOrgDto, DUMMY_ID);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void givenValidIdWhenAddOrgUserThenCreated() throws Exception {
        //Arrange
        doNothing().when(orgService).addUserToOrganization(addUserToOrgDto, DUMMY_ID);

        //Act
        mockMvc.perform(post(MAIN_URL + "/user/" + DUMMY_ID)
                        .content(convertObjectToString(addUserToOrgDto))
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        //Assert
        verify(orgService, times(1)).addUserToOrganization(addUserToOrgDto, DUMMY_ID);
    }

    private String convertObjectToString(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }


    private AddUserToOrgDto getRegisterOrgUse() {
        return AddUserToOrgDto.builder()
                .userId(1L)
                .build();
    }

    private OrganizationResponseInfoDto getOrgResponseInfoDto() {
        return OrganizationResponseInfoDto.builder()
                .id(1L)
                .name(NAME)
                .build();
    }

    private OrganizationCreateRequestDto getOrgRequestDto() {
        return OrganizationCreateRequestDto.builder()
                .name(NAME)
                .build();
    }

}
