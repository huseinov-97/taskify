package bn.abb.task.web.rest;


import bn.abb.task.domain.Authority;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.domain.enumeration.AuthEnum;
import bn.abb.task.dto.token.InteractionResponse;
import bn.abb.task.dto.user.RegisterRequestDto;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.impl.AuthServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@SpringBootTest
public class AuthControllerTest {


    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrganizationRepository organizationRepository;
    @Autowired
    private AuthorityRepository authRepository;

    private RegisterRequestDto regRequest;
    private User user;

    @Mock
    Set<User> mocUsers;

    @Before
    public void setup() {
        Authority auth1 = Authority.builder().id(1).name(AuthEnum.ADMIN).build();
        Authority auth2 = Authority.builder().id(2).name(AuthEnum.USER).build();

        Set<Authority> set = Stream.of(auth1, auth2)
                .collect(Collectors.toSet());

        Organization organization = Organization.builder().id(1L).name("ABB").user(mocUsers)
                .build();

        organizationRepository.save(organization);
        authRepository.save(auth1);
        authRepository.save(auth2);

        user = User.builder()
                .id(1L).name("Natig").surname("Bashir")
                .organization(organization)
                .password("HelloWorld")
                .authorities(set)
                .build();
        userRepository.save(user);

        regRequest = RegisterRequestDto.builder().build();

    }

    @Test
    public void registerUser() {
        ResponseEntity<InteractionResponse> ok = ResponseEntity.ok(new InteractionResponse("User registered successfully!"));

        AuthServiceImpl authService = Mockito.mock(AuthServiceImpl.class);
        Mockito.when(authService.registerUser(regRequest)).thenReturn(user);
        AuthController authController = new AuthController(authService);
        ResponseEntity<?> returnedValue = authController.registerUser(regRequest);
        Assert.assertEquals(returnedValue, ok);
    }
}