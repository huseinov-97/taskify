package bn.abb.task.web.rest;


import bn.abb.task.dto.user.RegisterUserDto;
import bn.abb.task.dto.user.UserDto;
import bn.abb.task.exception.EmailAlreadyUsedException;
import bn.abb.task.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static bn.abb.task.exception.EmailAlreadyUsedException.EMAIL_ALREADY_USED;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.ALL;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(UserController.class)
@SuppressWarnings("PMD.UnusedPrivateField")
public class UserControllerTest {

    private static final String MAIN_URL = "/user";
    private static final String CHARACTER_ENCODING_FORMAT = "UTF-8";
    private static final String ERROR_MESSAGE_TITLE = "$.message";
    private static final String EMAIL_VALUE = "mail@mail.ru";
    private static final String NAME = "First";
    private static final String SURNAME = "Last";
    private static final String PASSWORD = "102030";

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @MockBean
    private UserService userService;
    @Qualifier("jacksonObjectMapper")
    @Autowired
    private ObjectMapper mapper;

    private RegisterUserDto registerUserDto;
    private UserDto userDto;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();
        registerUserDto = RegisterUserDto.builder()
                .email(EMAIL_VALUE)
                .name(NAME)
                .surname(SURNAME)
                .password(PASSWORD)
                .build();
        userDto = UserDto.builder()
                .email(EMAIL_VALUE)
                .name(NAME)
                .surname(SURNAME)
                .build();

    }

    @Test
    public void givenRegisterUserDtoWhenCreateUserThenSuccess() throws Exception {
        //Act
        mockMvc.perform(post(MAIN_URL)
                        .contentType(APPLICATION_JSON)
                        .characterEncoding(CHARACTER_ENCODING_FORMAT)
                        .content(convertObjectToString(registerUserDto))
                        .accept(ALL))
                .andExpect(status().isOk());
    }


    @Test
    public void givenRegisterUserDtoWithAlreadyRegisteredEmailWhenCreateUserThenError() throws Exception {
        //Arrange
        doThrow(new EmailAlreadyUsedException(EMAIL_VALUE)).when(userService).create(any());

        //Act
        mockMvc.perform(post(MAIN_URL)
                        .contentType(APPLICATION_JSON)
                        .characterEncoding(CHARACTER_ENCODING_FORMAT)
                        .content(convertObjectToString(registerUserDto))
                        .accept(ALL))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath(ERROR_MESSAGE_TITLE).value(String.format(EMAIL_ALREADY_USED, EMAIL_VALUE)));
    }

    @Test
    public void givenLoggedInUserWhenGetCurrentUserInformationThenSuccess() throws Exception {
        //Arrange
        when(userService.getCurrentUser()).thenReturn(userDto);

        //Act
        mockMvc.perform(get(MAIN_URL)
                        .contentType(APPLICATION_JSON)
                        .characterEncoding(CHARACTER_ENCODING_FORMAT)
                        .accept(ALL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email").value(EMAIL_VALUE));
    }


    private String convertObjectToString(Object obj) throws JsonProcessingException {
        return mapper.writeValueAsString(obj);
    }

}
