package bn.abb.task.service;


import bn.abb.task.domain.Authority;
import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.domain.enumeration.AuthEnum;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.OrganizationRepository;
import bn.abb.task.repository.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private OrganizationRepository organizationRepository;

    @Mock
    private AuthorityRepository authRepository;

    @Mock
    private Set<User> mocUser;

    @Test
    public void registerUser() {
        Authority auth1 = Authority.builder().id(1).name(AuthEnum.ADMIN).build();
        Authority auth2 = Authority.builder().id(2).name(AuthEnum.USER).build();

        Set<Authority> set = Stream.of(auth1, auth2)
                .collect(Collectors.toSet());

        Organization organization = Organization.builder().id(1L).name("ABB").user(mocUser)
                .build();

        organizationRepository.save(organization);
        authRepository.save(auth1);
        authRepository.save(auth2);

        User user = User.builder()
                .id(1L).name("Natig").surname("Bashir")
                .email("natiqbashir@outlook.com")
                .organization(organization)
                .password("HelloWorld")
                .authorities(set)
                .build();
        userRepository.save(user);


        Assert.assertNotNull(userRepository.findByEmail("natiqbashir@outlook.com"));
        Assert.assertNotNull(authRepository.findByName(AuthEnum.USER));
    }
}
