package bn.abb.task.service;


import bn.abb.task.domain.Organization;
import bn.abb.task.domain.User;
import bn.abb.task.dto.user.RegisterUserDto;
import bn.abb.task.dto.user.UserDto;
import bn.abb.task.exception.EmailAlreadyUsedException;
import bn.abb.task.exception.UserNotFoundException;
import bn.abb.task.mapper.UserMapper;
import bn.abb.task.repository.AuthorityRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.security.SecurityService;
import bn.abb.task.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;

import static bn.abb.task.exception.EmailAlreadyUsedException.EMAIL_ALREADY_USED;
import static bn.abb.task.exception.UserNotFoundException.USER_NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

    private static final String EMAIL = "mail@mail.ru";
    private static final String NAME = "First";
    private static final String SURNAME = "Last";
    private static final String PASSWORD = "102030Aa*";

    @Mock
    private UserRepository userRepository;
    @Mock
    private AuthorityRepository authRepository;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private UserMapper userMapper;
    @Mock
    private SecurityService securityService;

    @Mock
    private Organization mocOrg;

    @Mock
    private User mocUser;

    @InjectMocks
    private UserServiceImpl userService;

    private User user;
    private Organization organization;
    private RegisterUserDto registerUserDto;
    private UserDto userDto;


    @BeforeEach
    public void setUp() {

        user = User.builder()
                .id(1L)
                .email(EMAIL)
                .name(NAME)
                .surname(SURNAME)
                .organization(mocOrg)
                .build();

        registerUserDto = RegisterUserDto.builder()
                .email(EMAIL)
                .password(PASSWORD)
                .name(NAME)
                .surname(SURNAME)
                .build();
        userDto = UserDto.builder()
                .name(NAME)
                .surname(SURNAME)
                .email(EMAIL)
                .organization(mocOrg)
                .build();

        organization = getOrganization();
    }

    @Test
    public void givenRegisterUserDtoWhenCreateUserThenSuccess() {
        //Arrange
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());
        when(userMapper.dtoToEntity(any())).thenReturn(user);
        when(userRepository.save(any())).thenReturn(user);
        when(authRepository.save(any())).thenReturn(Optional.empty());

        //Act
        userService.create(registerUserDto);

        //Assert
        assertThat(user.getAuthorities()).isNotNull();
        assertThat(user.getEmail()).isEqualTo(EMAIL);
        assertThat(user.getName()).isEqualTo(NAME);
        assertThat(user.getOrganization()).isNotNull();
    }

    @Test
    public void givenRegisterUserDtoWithAlreadyRegisteredEmailWhenCreateUserThenError() {
        //Arrange
        when(userRepository.findByEmail(any())).thenReturn(Optional.of(user));

        //Act & Assert
        assertThatThrownBy(() -> userService.create(registerUserDto)).isInstanceOf(EmailAlreadyUsedException.class)
                .hasMessage(String.format(EMAIL_ALREADY_USED, EMAIL));
    }


    @Test
    public void whenGetCurrentUserThenSuccess() {
        //Arrange
        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(EMAIL));
        when(userRepository.findByEmail(any())).thenReturn(Optional.of(user));
        when(userMapper.entityToDto(any())).thenReturn(userDto);

        //Act
        UserDto currentUser = userService.getCurrentUser();

        //Assert
        assertThat(currentUser.getEmail()).isEqualTo(EMAIL);
        assertThat(user.getEmail()).isEqualTo(EMAIL);
        assertThat(user.getName()).isEqualTo(NAME);
    }

    @Test
    public void whenGetCurrentUserLoginThenNotFound() {
        //Arrange
        when(securityService.getCurrentUserLogin()).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> userService.getCurrentUser()).isInstanceOf(UserNotFoundException.class)
                .hasMessage(USER_NOT_FOUND);
    }

    @Test
    public void whenGetCurrentUserThenUserNotFound() {
        //Arrange
        when(securityService.getCurrentUserLogin()).thenReturn(Optional.of(EMAIL));
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());

        //Act & Assert
        assertThatThrownBy(() -> userService.getCurrentUser()).isInstanceOf(UserNotFoundException.class)
                .hasMessage(USER_NOT_FOUND);
    }


    private Organization getOrganization() {
        return Organization.builder()
                .id(1L)
                .name(NAME)
                .user(Set.of(user))
                .build();
    }

}
