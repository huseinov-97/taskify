package bn.abb.task.service;

import bn.abb.common.dto.GenericSearchDto;
import bn.abb.common.exception.NotFoundException;
import bn.abb.common.search.SearchSpecification;
import bn.abb.task.domain.Task;
import bn.abb.task.domain.enumeration.TaskStatus;
import bn.abb.task.dto.task.TaskCreateRequestDto;
import bn.abb.task.dto.task.TaskResponseDto;
import bn.abb.task.exception.NameMustBeUniqueException;
import bn.abb.task.repository.TaskRepository;
import bn.abb.task.repository.UserRepository;
import bn.abb.task.service.impl.TaskServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class TaskServiceImplTest {

    private static final Long DUMMY_ID = 1L;
    private static final String DUMMY_STRING = "string";
    private static final LocalDateTime DUMMY_DATETIME = LocalDateTime.parse("2015-02-20T06:30:00");
    private static final String TASK_TITLE_MUST_BE_UNIQUE = "This name called string has already been used.";

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDetails userDetails;

    @Spy
    private ModelMapper modelMapper;

    private TaskCreateRequestDto taskRequestDto;

    private TaskResponseDto taskResponseDto;

    private Task task;


    @BeforeEach
    void setUp() {
        taskRequestDto = getTaskRequestDto();

        taskResponseDto = getTaskResponseDto();

        task = getTask();
    }

    @Test
    void givenCourseRequestDtoWhenCreateThenCourseResponseDto() {

        //Arrange
        final TaskResponseDto taskResponseDto = modelMapper.map(task, TaskResponseDto.class);
        when(taskRepository.save(task)).thenReturn(task);

        //Act
        TaskResponseDto result = taskService.create(taskRequestDto, userDetails);

        //Assert
        assertThat(result).isEqualTo(taskResponseDto);
    }

    @Test
    public void givenRegisterOrganizationRequestDtoWhenCreateOrganizationThenException() {
        //Arrange
        when(taskRepository.findByTitle(any())).thenReturn(Optional.of(taskRepository));


        //Act & Assert
        assertThatThrownBy(() -> taskService.create(taskRequestDto, userDetails))
                .isInstanceOf(NameMustBeUniqueException.class)
                .hasMessage(String.format(TASK_TITLE_MUST_BE_UNIQUE, DUMMY_STRING));
    }

    @Test
    void givenGenericSearchDtoWhenSearchThenExceptCourseResponseDto() {
        //Arrange
        when(taskRepository.findAll(any(SearchSpecification.class), any(Pageable.class)))
                .thenReturn(Page.empty());

        //Act
        taskService.search(new GenericSearchDto(), Pageable.unpaged());

        //Assert
        verify(taskRepository, times(1))
                .findAll(any(SearchSpecification.class), any(Pageable.class));
    }

    @Test
    void givenInValidTaskIdWhenDeleteThenException() {

        //Act & Assert
        assertThatThrownBy(() -> taskService.delete(DUMMY_ID)).isInstanceOf(NotFoundException.class);
    }

    @Test
    void givenValidCourseIdWhenDeleteThenOk() {
        //Act
        taskService.delete(DUMMY_ID);

        //Verify
        verify(taskRepository, times(1)).deleteById(DUMMY_ID);
    }


    private TaskResponseDto getTaskResponseDto() {
        return TaskResponseDto
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .build();
    }

    private Task getTask() {
        return Task
                .builder()
                .id(DUMMY_ID)
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .build();
    }

    private TaskCreateRequestDto getTaskRequestDto() {
        return TaskCreateRequestDto
                .builder()
                .title(DUMMY_STRING)
                .description(DUMMY_STRING)
                .status(TaskStatus.ONGOING)
                .deadline(DUMMY_DATETIME)
                .build();
    }


}
